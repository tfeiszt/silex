<?php
namespace tfeiszt\silex;

use Silex\Application as SilexApp;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Application
 * @package tfeiszt\silex
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Application extends SilexApp
{
    use \Silex\Application\TranslationTrait;

    /**
     * @param Application $app
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function hasLocale(Application $app)
    {
        return isset($app['locale.listener']);
    }

    /**
     * @param Request $request
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getControllerClass(Request $request)
    {
        $segment = $this->getControllerSegment($request);
        return $controllerClass = $this->getControllerClassBySegment($segment);
    }

    /**
     * @param $segment
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getControllerClassBySegment($segment)
    {
        $controllerClass = ($segment == '') ? 'index' : $this->getClassNameOfSegment($segment);
        return '\App\controller\\' . ucwords($controllerClass) . 'Controller';
    }

    /**
     * @param $segment
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getClassNameOfSegment($segment)
    {
        return str_replace(' ', '', ucwords(str_replace(['_', '-'], ' ', $segment)));
    }

    /**
     * @param $segment
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getRoutePrefixBySegment($segment)
    {
        return ($segment == '') ? '/' : '/' . strtolower($segment);
    }

    /**
     * @param Request $request
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getControllerSegment(Request $request)
    {
        $segment = '';
        $controllerIndex = 1;
        if ($uri = $request->getRequestUri()) {
            if (trim($uri) != '/') {
               if ($segments = explode('/', $uri)) {
                   if (static::hasLocale($this)) {
                       $controllerIndex = 2;
                   }
                   return (isset($segments[$controllerIndex])) ? $segments[$controllerIndex] : $segments[$controllerIndex-1];
               }
               return $uri;
            }
        }
        return $segment;
    }
}
