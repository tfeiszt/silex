<?php

namespace tfeiszt\silex\controller;

use OAuth2\TokenType\Bearer;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use tfeiszt\helper\Helper;
use tfeiszt\helper\UrlHelper;
use tfeiszt\silex\model\Meta;
use tfeiszt\silex\model\Collection;
use tfeiszt\silex\model\Error;
use tfeiszt\silex\model\Record;

/**
 * Class AbstractResourceController
 * @package tfeiszt\silex\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Abstract class AbstractResourceController extends AbstractController implements ControllerProviderInterface
{
    /**
     * @var string
     */
    protected static $_modelClass;
    /**
     * @var []
     */
    protected static $_resourceGroups = [];

    /**
     * @param Application $app
     * @return null
     */
    public function connect(Application $app)
    {
        $controller = parent::connect($app);
        /**
         * Register basic rest methods
         */
        $verifyTokenClosure = function (Request $request, Application $app) {
            // checking pre-configured tokens
            $oauthRequest = \OAuth2\Request::createFromGlobals();
            $tokenType = new Bearer();
            if ($tokenType->requestHasToken($oauthRequest)) {
                if ($token = $tokenType->getAccessTokenParameter($oauthRequest, new \OAuth2\Response())) {
                    if (in_array($token, $app['service.oauth.pre-configured-tokens'])) {
                        return;
                    }
                }
            }
            // checking normal tokens
            if (!$app['service.oauth.server']->verifyResourceRequest($oauthRequest)) {
                /** @var \OAuth2\Response $response */
                $response = $app['service.oauth.server']->getResponse();
                $error = new Error($response->getStatusText(), $response->getStatusCode());
                return $app['output.json']($error);
                die();
            }
        };

        /**
         * Register cache control for GET requests
         */
        $cacheControlClosure = function (Request $request, Response $response) use ($app) {
            $response = $app['http_cache.strategy']($request, $response);
        };

        $cid = Helper::getClassShortName(static::class);
        $controller->get("/", array( $this, 'index' ) )->before($verifyTokenClosure)->after($cacheControlClosure)->bind( $cid . '_list' );
        $controller->get("/{id}/", array( $this, 'retrieve' ) )->before($verifyTokenClosure)->after($cacheControlClosure)->bind( $cid . '_retrieve' );
        $controller->post("/", array( $this, 'create' ) )->before($verifyTokenClosure)->bind( $cid . '_create' );
        $controller->put("/{id}/", array( $this, 'update') )->before($verifyTokenClosure)->bind( $cid . '_full_update');
        $controller->patch("/{id}/", array( $this, 'update') )->before($verifyTokenClosure)->bind( $cid . '_part_update');
        $controller->delete("/{id}/", array( $this, 'delete') )->before($verifyTokenClosure)->bind( $cid . '_delete');
        return $controller;
    }

    /**
     * @param Application $app
     * @param string $modelClass
     * @return \tfeiszt\silex\model\ORMModelInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected function modelFactory($app, $modelClass = '')
    {
        $className = static::$_modelClass;
        if (!empty($modelClass) && class_exists($modelClass)) {
            $className = $modelClass;
        }
        return new $className($app, $app['orm.ems'][$className::getDb()]);
    }

    /**
    * @param string $name
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    protected static function getNormalizedSchema($name)
    {
        if (isset(static::$_resourceGroups[$name])) {
            return static::$_resourceGroups[$name];
        }
        return ['default'];
    }

    /**
     * @param Application $app
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function index(Application $app)
    {
        $request = Request::createFromGlobals();

        $model = $this->modelFactory($app);

        $criteria = UrlHelper::getCriteriaByRequest($request, $model::getAllowedCriteria());

        $result = [];
        if ($listResult = $model->findByCriteria($criteria)) {
            foreach ($listResult->getItems() as $entity) {
                $result[] = $entity->normalize(static::getNormalizedSchema('retrieve'));
            }
        }

        return $app['output.json'](new Collection($result, new Meta($listResult->getMeta())), 200);
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function retrieve(Application $app, $id)
    {
        $model = $this->modelFactory($app);
        try {
            if (!$entity = $model->findById($id)) {
                throw new \Exception(Helper::getResponseMessageByCode(404), 404);
            }
        } catch (\Exception $e) {
            return $app['output.json'](new Error($e->getMessage(), $e->getCode()));
        }
        return $app['output.json'](new Record($entity->normalize(static::getNormalizedSchema('retrieve'))));
    }

    /**
     * @param Application $app
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function create(Application $app)
    {
        $request = Request::createFromGlobals();
        $model = $this->modelFactory($app);

        if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
        $data = $request->request->all();

        try {
            if ($entity = $model::entityFactory()) {
                if ($model->validate($data)) {
                    if (!$entity = $model->setEntityAndSave($entity, $data, true)) {
                        throw new \Exception('An Error Occurred', 500);
                    }
                } else {
                    throw new \Exception($model->getValidationMessages(), 400);
                }
            }
        } catch(\Exception $e) {
            return $app['output.json'](new Error($e->getMessage(), $e->getCode()));
        }

        return $app['output.json'](new Record($entity->normalize(static::getNormalizedSchema('retrieve'))), 201);
    }

    /**
     * @param Application $app
     * @param $id
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function update(Application $app, $id)
    {
        $model = $this->modelFactory($app);
        $request = Request::createFromGlobals();
        if (strpos($request->headers->get('Content-Type'), 'application/json') === 0) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
        $data = $request->request->all();
        try {
            if ($entity = $model->findById($id)) {
                if ($model->validate($data)) {
                    if (!$model->setEntityAndSave($entity, $data)) {
                        throw new \Exception('An Error Occurred', 500);
                    }
                } else {
                    throw new \Exception($model->getValidationMessages(), 400);
                }
            }
        } catch(\Exception $e) {
            return $app['output.json'](new Error($e->getMessage(), $e->getCode()));
        }

        return $app['output.json'](new Record($entity->normalize(static::getNormalizedSchema('retrieve'))), 202);
    }

    /**
     * @param Application $app
     * @param $id
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function delete(Application $app, $id)
    {
        $model = $this->modelFactory($app);
        try {
            if ($entity = $model->findById($id)) {
                $model->remove($entity);
            } else {
                throw new \Exception(Helper::getResponseMessageByCode(404), 404);
            }
        } catch (\Exception $e) {
            return $app['output.json'](new Error($e->getMessage(), $e->getCode()));
        }
        return $app['output.json'](new Record([]), 204);
    }
}
