<?php

namespace tfeiszt\silex\controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
/**
 * Class AbstractPageController
 * @package tfeiszt\silex\controller
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Abstract class AbstractController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     * @return null
     */
    public function connect(Application $app)
    {
        return $app['controllers_factory'];
    }
}
