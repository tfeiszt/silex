<?php
namespace tfeiszt\silex\model;

use Silex\Application;
use JsonSerializable;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class AbstractModel
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Abstract class AbstractModel implements JsonSerializable
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    protected $validationErrors = [];

    /**
     * AbstractModel constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = self::getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return $this
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            }
        }
        return $this;
    }

    /**
     * @param string $format
     * @return bool|float|int|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function serialize($format = 'json')
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($this, $format);
    }

    /**
     * @return bool|float|int|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function jsonSerialize()
    {
        return $this->serialize('json');
    }

    /**
     * @param $data
     * @return bool
     */
    public function validate($data)
    {
        $constraints = static::getConstraints();

        $this->validationErrors = $this->app['validator']->validate($data, $constraints);
        if (count($this->validationErrors) >0) {
            foreach ($this->validationErrors as $error)
                $this->app['monolog']->addDebug($error);
        }
        return count($this->validationErrors) == 0;
    }

    /**
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * @return string
     */
    public function getValidationMessages()
    {
        $message = '';
        foreach ($this->validationErrors as $error) {
            $message .= $error->getPropertyPath().' '.$error->getMessage()."\n";
        }
        return $message;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public static function getConstraints();

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public static function getDefaultValues();
}
