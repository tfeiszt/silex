<?php
namespace tfeiszt\silex\model;

/**
 * Class AbstractListResult
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Abstract class AbstractListResult implements ListResultInterface
{
    /**
     * @var array
     */
    public $items = [];

    /**
     * @return \Generator
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getItems()
    {
        foreach ($this->items as $item) {
            yield $item;
        }
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function getMeta();
}
