<?php
namespace tfeiszt\silex\model;

/**
 * Class Collection
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Class Collection extends AbstractResponse
{
    /**
     * @var array
     */
    public $items = [];
    /**
     * @var null|Meta
     */
    public $meta = null;

    /**
     * Collection constructor.
     * @param array $items
     * @param Meta $meta
     */
    public function __construct($items = [], Meta $meta)
    {
        $this->items = $items;
        $this->meta = $meta;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * @return null|Meta
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function toArray()
    {
        return [
            'collection' => [
                'items' => $this->getItems(),
                'meta' => $this->getMeta()->data
            ]
        ];
    }
}
