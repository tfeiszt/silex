<?php
namespace tfeiszt\silex\model;

use JsonSerializable;

/**
 * Class AbstractOptions
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
abstract class AbstractOptions implements JsonSerializable
{
    /**
     * ListerOptions constructor.
     * @param array $options
     */
    public function __construct($options = [])
    {
        foreach(get_object_vars($this) as $key => $item) {
            if (isset($options[$key])) {
                $this->{$key} = $options[$key];
            }
        }
    }

    /**
     * @param $key
     * @return mixed
     * @throws \Exception
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function get($key)
    {
        if (property_exists($this, $key)) {
            return $this->{$key};
        } else {
            throw new \Exception('Property does not exist on class [' . get_class($this) . ']');
        }
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function jsonSerialize()
    {
        return json_encode(get_object_vars($this));
    }
}
