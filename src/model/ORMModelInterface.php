<?php
namespace tfeiszt\silex\model;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;

interface ORMModelInterface
{
    /**
     * @param $id
     * @return AbstractAnnotation
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findById($id);

    /**
     * @param Criteria $criteria
     * @return AbstractAnnotation[]|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findAll(Criteria $criteria);

    /**
     * @param Criteria $criteria
     * @return AbstractAnnotation|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findFirst(Criteria $criteria);

    /**
     * @param Criteria $criteria
     * @param int $limit
     * @param int $offset
     * @return ListResultInterface|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findByCriteria(Criteria $criteria, $limit = 20, $offset = 0);

    /**
     * @param Criteria $criteria
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function countByCriteria(Criteria $criteria);

    /**
     * @param QueryBuilder $qb
     * @param array $params
     * @param int $limit
     * @param int $offset
     * @return ListResultInterface|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findByDQL(QueryBuilder $qb, $params = [], $limit = 20, $offset = 0);

    /**
     * @param QueryBuilder $qb
     * @param array $params
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function countByDQL(QueryBuilder $qb, $params = []);

    /**
     * @param AbstractAnnotation $entity
     * @param array $data
     * @param bool $useDefaults
     * @return AbstractAnnotation
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function setEntityAndSave(AbstractAnnotation $entity, $data = [], $useDefaults = false);

    /**
     * @param AbstractAnnotation $entity
     * @return void
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function remove(AbstractAnnotation $entity);

    /**
     * @return AbstractAnnotation
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function entityFactory();

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getDb();

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getAnnotationClass();
}
