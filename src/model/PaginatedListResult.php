<?php
namespace tfeiszt\silex\model;

/**
 * Class PaginatedListResult
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class PaginatedListResult extends AbstractListResult implements ListResultInterface
{
    /**
     * @var int
     */
    public $pageSize;
    /**
     * @var int
     */
    public $offset;
    /**
     * @var int
     */
    public $countAll;

    /**
     * PaginatedListResult constructor.
     * @param $items
     * @param $countAll
     * @param $pageSize
     * @param int $offset
     */
    public function __construct($items, $countAll, $pageSize, $offset = 0)
    {
        $this->items = $items;
        $this->countAll = $countAll;
        $this->pageSize = $pageSize;
        $this->offset = $offset;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getMeta()
    {
        return [
            'pagination' => [
                'current_page_size' => count($this->items),
                'page_size' => $this->pageSize,
                'offset' => $this->offset,
                'count_all' => $this->countAll
            ]
        ];
    }
}
