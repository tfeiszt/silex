<?php
namespace tfeiszt\silex\model;

/**
 * Class Record
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Class Record extends AbstractResponse
{
    /**
     * @var array
     */
    public $data = [];

    /**
     * Record constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function toArray()
    {
        return [
            'record' => $this->getData()
        ];
    }
}
