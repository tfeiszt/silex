<?php
namespace tfeiszt\silex\model;

/**
 * Class Error
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Class Error extends AbstractResponse
{
    /**
     * @var string
     */
    public $message;

    /**
     * @var int
     */
    public $code;

    /**
     * @var array
     */
    public $context = [];

    /**
     * Error constructor.
     * @param $message
     * @param $code
     * @param array $context]
     */
    public function __construct($message, $code, $context = [])
    {
        $this->message = $message;
        $this->code = $code;
        $this->context = $context;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function toArray()
    {
        return [
            'error' => [
                'message' => $this->message,
                'code' => $this->code,
                'context' => $this->context
            ]
        ];
    }
}
