<?php
namespace tfeiszt\silex\model;

/**
 * Class Meta
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Class Meta
{
    public $data = [];

    /**
     * Meta constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * @param $key
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function hasKey($key)
    {
        return isset($this->data[$key]);
    }

    /**
     * @param $name
     * @return mixed|null
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function __get($name)
    {
        if ($this->hasKey($name)) {
            return $this->data[$name];
        }
        return null;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
        return $this;
    }
}
