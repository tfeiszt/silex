<?php
namespace tfeiszt\silex\model;

/**
 * Interface ListResultInterface
 * @package tfeiszt\silex\model
 */
interface ListResultInterface
{
    /**
     * @return \Generator
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getItems();

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getMeta();
}
