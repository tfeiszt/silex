<?php
namespace tfeiszt\silex\model;

/**
 * Class CursorListResult
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class CursorListResult extends AbstractListResult implements ListResultInterface
{
    /**
     * @var int
     */
    public $pageSize;
    /**
     * @var int
     */
    public $cursor;

    /**
     * CursorListResult constructor.
     * @param $items
     * @param $pageSize
     * @param int $cursor
     */
    public function __construct($items, $pageSize, $cursor = 0)
    {
        $this->items = $items;
        $this->pageSize = $pageSize;
        $this->cursor = $cursor;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getMeta()
    {
        return [
            'pagination' => [
                'current_page_size' => count($this->items),
                'page_size' => $this->pageSize,
                'cursor' => $this->cursor
            ]
        ];
    }
}
