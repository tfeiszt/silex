<?php
namespace tfeiszt\silex\model;

use Doctrine\ORM\QueryBuilder;
use Silex\Application;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Trait ORMTrait
 * @package tfeiszt\silex\model
 */
trait ORMTrait
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    use ValidationTrait;

    /**
     * ORMTrait constructor.
     * @param Application $app
     * @param EntityManagerInterface $em
     */
    public function __construct(Application $app, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->app = $app;
        $this->em = $em;
    }

    /**
     * @return AbstractAnnotation
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function entityFactory()
    {
        $className = static::getAnnotationClass();
        return new $className();
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getAnnotationClass()
    {
        return static::$_annotation;
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getDb()
    {
        return static::$_db;
    }

    /**
     * @param $id
     * @return AbstractAnnotation
     */
    public function findById($id)
    {
        return $this->em->find(static::getAnnotationClass(), $id);
    }

    /**
     * @param Criteria $criteria
     * @return AbstractAnnotation[]|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findAll(Criteria $criteria)
    {
        if (($data = $this->em->getRepository(static::getAnnotationClass())->matching($criteria)) && count($data)) {
            return $data;
        }
        return false;
    }

    /**
     * @param Criteria $criteria
     * @return AbstractAnnotation|false
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findFirst(Criteria $criteria)
    {
        if (($data = $this->em->getRepository(static::getAnnotationClass())->matching($criteria)) && count($data)) {
            return $data[0];
        }
        return false;
    }

    /**
     * @param Criteria $criteria
     * @param int $limit
     * @param int $offset
     * @return ListResultInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findByCriteria(Criteria $criteria, $limit = 20, $offset = 0)
    {
        $countAll = $this->countByCriteria($criteria);
        if ($countAll) {
            if ($limit > 0) {
                $criteria->setMaxResults($limit)
                    ->setFirstResult($offset);
            }
            /** @var \Doctrine\ORM\LazyCriteriaCollection $data */
            $data = $this->em->getRepository(static::getAnnotationClass())->matching($criteria);
            $result = new PaginatedListResult($data->toArray(), $countAll, $limit, $offset);
        } else {
            $result = new PaginatedListResult([], $countAll, $limit, $offset);
        }
        return $result;
    }

    /**
     * @param Criteria $criteria
     * @return int
     */
    public function countByCriteria(Criteria $criteria)
    {
        /** @var \Doctrine\ORM\LazyCriteriaCollection $data */
        if ($data = $this->em->getRepository(static::getAnnotationClass())->matching($criteria)) {
            return $data->count();
        }
        return 0;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     * @param int $limit
     * @param int $offset
     * @return ListResultInterface
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function findByDQL(QueryBuilder $qb, $params = [], $limit = 20, $offset = 0)
    {
        $countAll = $this->countByDQL($qb, $params);
        if ($countAll) {
            if ($limit > 0) {
                $qb->setMaxResults($limit)
                    ->setFirstResult($offset);
            }
            $data = $qb->getQuery()->setParameters($params)->getResult();
            $result = new PaginatedListResult($data, $countAll, $limit, $offset);
        } else {
            $result = new PaginatedListResult([], $countAll, $limit, $offset);
        }
        return $result;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     * @return int
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function countByDQL(QueryBuilder $qb, $params = [])
    {
        return count($qb->setParameters($params)->getQuery()->getResult());
    }

    /**
     * @param AbstractAnnotation $entity
     * @param array $data
     * @param bool $useDefaults
     * @return AbstractAnnotation|false
     */
    public function setEntityAndSave(AbstractAnnotation $entity, $data = [], $useDefaults = false)
    {
        $entity->setData($data, $useDefaults);
        try {
            if (!$entity->getId()) {
                $this->em->persist($entity);
                $this->em->flush();
                $this->em->clear();
            } else {
                $this->em->merge($entity);
                $this->em->flush();
                $this->em->clear();
            }
        } catch (\Exception $e) {
            if (isset($this->app['monolog'])) {
                $this->app['monolog']->addError($e->getMessage());
            }
            return false;
        }
        return $entity;
    }

    /**
     * @param AbstractAnnotation $entity
     * @return void
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function remove(AbstractAnnotation $entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }
}
