<?php
namespace tfeiszt\silex\model;

use JsonSerializable;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
/**
 * Class AbstractModel
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Abstract class AbstractAnnotation implements JsonSerializable
{
    /**
     * AbstractAnnotation constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $key
     * @return string|null
     */
    public function getDefaultValue($key)
    {
        $defaults = $this->getDefaultValues();
        if (isset($defaults[$key])) {
            if (is_callable($defaults[$key])) {
                return $defaults[$key]();
            } else {
                return $defaults[$key];
            }
        } else {
            return null;
        }
    }

    /**
     * @param $data
     * @param bool $useDefaults
     * @return $this
     */
    public function setData($data, $useDefaults = false)
    {
        foreach (get_object_vars($this) as $key => $prop) {
            if (isset($data[$key])) {
                if ($data[$key] == '' || $data[$key] === null) {
                    if ($useDefaults === true) {
                        $this->{$key} = $this->getDefaultValue($key);
                    }
                } else {
                    $this->{$key} = $data[$key];
                }
            } else {
                if ($useDefaults === true) {
                    $this->{$key} = $this->getDefaultValue($key);
                }
            }
        }
        return $this;
    }

    /**
     * @param array $groups
     * @return array|bool|float|int|object|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function normalize($groups = ['default'])
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $normalizer = new ObjectNormalizer($classMetadataFactory);
        $serializer = new Serializer(array($normalizer));

        $data = $serializer->normalize($this, null, ['groups' => $groups]);
        return $data;
    }

    /**
     * @param string $format
     * @return bool|float|int|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function serialize($format = 'json')
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($this, $format);
    }

    /**
     * @return bool|float|int|string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function jsonSerialize()
    {
        return $this->serialize('json');
    }

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public static function getConstraints();

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public static function getDefaultValues();

    /**
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public static function getAllowedCriteria();
}
