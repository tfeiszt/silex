<?php
namespace tfeiszt\silex\model;

/**
 * Class AbstractResponse
 * @package tfeiszt\silex\model
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
Abstract class AbstractResponse
{
    /**
     * @return []
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    abstract public function toArray();
}
