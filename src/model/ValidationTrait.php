<?php
namespace tfeiszt\silex\model;
use tfeiszt\helper\Helper;

/**
 * Trait ValidationTrait
 * @package App\model
 */
trait ValidationTrait
{
    /**
     * @var \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    public $validationErrors = [];

    /**
     * @param $data
     * @return bool
     */
    public function validate($data)
    {
        $constraints = static::getConstraints();

        $this->validationErrors = $this->app['validator']->validate($data, $constraints);
        if (count($this->validationErrors) >0) {
            foreach ($this->validationErrors as $error)
                $this->app['monolog']->addDebug($error);
        }
        return count($this->validationErrors) == 0;
    }

    /**
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface[]
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * @param string $glue
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function getValidationMessages($glue = "\n")
    {
        $message = '';
        $sep = '';
        foreach ($this->validationErrors as $error) {
            $message .= $sep . $error->getMessage() . ' [' . ucwords(Helper::underscore($error->getPropertyPath(), ' ')) . ']';
            $sep = $glue;
        }
        return $message;
    }
}
