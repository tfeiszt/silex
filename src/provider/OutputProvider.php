<?php

namespace tfeiszt\silex\provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;
use tfeiszt\silex\model\Collection;
use tfeiszt\silex\model\Error;
use tfeiszt\silex\model\Record;

/**
 * Class OutputProvider
 * @package tfeiszt\silex\provider
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class OutputProvider implements ServiceProviderInterface {

    /**
     * @param Container $app
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function register(Container $app) {
        $app['output.json'] = $app->protect(function($data, $code = 200, $headers = ['Content-Type' => 'application/json']) use ($app) {
            if ($data instanceof Collection) {
                return $app->json($data->toArray(), $code, $headers);
            } elseif ($data instanceof Record) {
                return $app->json($data->toArray(), $code, $headers);
            } elseif ($data instanceof Error) {
                return $app->json($data->toArray(), ($data->code) ? $data->code : 500, $headers);
            } elseif ($data instanceof \Exception) {
                $code = $data->getCode();
                if (!$code) {
                    if (method_exists($data,'getStatusCode')) {
                        if (!$code = $data->getStatusCode()) {
                            $code = 500;
                        }
                    } else {
                        $code = 500;
                    }
                }
                return $app->json((new Error($data->getMessage(), $code))->toArray(), $code, $headers);
            } else {
                return $app->json($data, $code, $headers);
            }
        });

        $app['output.render'] = $app->protect(function ($template, $data = []) use ($app) {
            if ($data instanceof Error) {
                $templates = array(
                    '/errors/' . $data->code . '.html.twig',
                    '/errors/' . substr($data->code, 0, 2) . 'x.html.twig',
                    '/errors/' . substr($data->code, 0, 1) . 'xx.html.twig',
                    '/errors/default.html.twig',
                );
                return new Response($app['twig']->resolveTemplate($templates)->render(['code' => $data->code, 'message' => $data->message]), $data->code);
            } elseif ($data instanceof \Exception) {
                $code = $data->getCode();
                if (!$code) {
                    if (method_exists($data,'getStatusCode')) {
                        if (!$code = $data->getStatusCode()) {
                            $code = 500;
                        }
                    } else {
                        $code = 500;
                    }
                }
                $templates = array(
                    '/errors/' . $code . '.html.twig',
                    '/errors/' . substr($code, 0, 2) . 'x.html.twig',
                    '/errors/' . substr($code, 0, 1) . 'xx.html.twig',
                    '/errors/default.html.twig',
                );

                return new Response($app['twig']->resolveTemplate($templates)->render(['code' => $code, 'message' => $data->getMessage()]), $code);
            }  else {
                return $app['twig']->render($template . '.html.twig', array('content' => $data));
            }
        });

        return;
    }
}
