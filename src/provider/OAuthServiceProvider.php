<?php

namespace tfeiszt\silex\provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use OAuth2\Storage\Pdo;
use OAuth2\Server;
use OAuth2\GrantType\ClientCredentials;
use OAuth2\GrantType\AuthorizationCode;
use OAuth2\HttpFoundationBridge\Response as BridgeResponse;

/**
 * Class OAuthServiceProvider
 * @package tfeiszt\silex\provider
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class OAuthServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $app
     */
    public function register(Container $app)
    {
        $app['service.oauth.server'] = function($app) {

            switch ($app['oauth.config']['storage']) {
                case 'pdo' :
                    $storage =new Pdo(array('dsn' => $app['oauth.config']['connection']['dsn'],
                        'username' => $app['oauth.config']['connection']['user'],
                        'password' => $app['oauth.config']['connection']['password']));
                    break;
                default:
                    $storage =new Pdo(array('dsn' => $app['oauth.config']['connection']['dsn'],
                        'username' => $app['oauth.config']['connection']['user'],
                        'password' => $app['oauth.config']['connection']['password']));
                    break;
            }

            // Pass a storage object or array of storage objects to the OAuth2 server class
            $server = new Server($storage);

            // Add the "Client Credentials" grant type (it is the simplest of the grant types)
            $server->addGrantType(new ClientCredentials($storage));

            // Add the "Authorization Code" grant type (this is where the oauth magic happens)
            $server->addGrantType(new AuthorizationCode($storage));

            return $server;
        };
        $app['service.oauth.response'] = new BridgeResponse();

        // Put fixed keys for own service structure
        $app['service.oauth.pre-configured-tokens'] = (isset($app['oauth.config']['tokens']) && !(empty($app['oauth.config']['tokens']))) ? $app['oauth.config']['tokens'] : [];

        return;
    }
}
