# Silex 

### PHP library description
Silex based microservice abstraction

  
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://tfeiszt@bitbucket.org/tfeiszt/silex.git"
    }
  ],
  "require": {
    "tfeiszt/silex": "dev-master"
  }
}
```
 
### License

MIT
